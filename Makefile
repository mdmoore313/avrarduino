#Makefile for helloworld project

DEVDIR=/home/pi/Documents/avr/
BINDIR=./bin/
SRCDIR=./src/

TARGET=$(BINDIR)/helloworld
C_SOURCE=
CPP_SOURCE=helloworld.cpp

SRC=
PSRC=$(addprefix $(SRCDIR)/, $(CPP_SOURCE))

include $(DEVDIR)/AVR-makefile-base.mk
